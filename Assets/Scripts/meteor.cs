﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meteor : MonoBehaviour
{
    // Start is called before the first frame update

    Vector2 speed;
    public GameObject[] sprites;
    int seleccionado;
    public AudioSource meteorito;

    void Awake()
    {
        for (int i=0; i<sprites.Length; i++)
        {
            sprites[i].SetActive(false);
        }

        seleccionado = Random.Range(0, sprites.Length);
        sprites[seleccionado].SetActive(true);

        speed.x = Random.Range(-8, 0);
        speed.y = Random.Range(-5, 5);
    }
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
        sprites[seleccionado].transform.Rotate(0, 0, 100 * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
         if (other.tag == "finish")
         {
             Destroy(this.gameObject);
         }
         else if (other.tag == "Bullet")
         {
             StartCoroutine(DestroyMeteor());
         }
    }
 
    IEnumerator DestroyMeteor()
    {
        //Desactivo el grafico
        sprites[seleccionado].SetActive(false);

        //Destruye BoxCollider
        Destroy(GetComponent<BoxCollider2D>());

        //Lanzo sonido de explosion
        meteorito.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        //Me destruyo a mi mismo
        Destroy(this.gameObject);

    }    
}
