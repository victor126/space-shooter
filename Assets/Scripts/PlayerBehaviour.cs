﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{

    public float speed;
	public Vector2 limits;
    private Vector2 axis;
    public Weapon weapon;
    public float ShotTime = 0;
	public Propeller prop;
    public float 

    void Update()
    {
        ShotTime += Time.deltaTime;

        transform.Translate(axis * speed * Time.deltaTime);

		if(transform.position.y > limits.y)
		{
			transform.position = new Vector3(transform.position.x, limits.y, transform.position.z);
		}
		else if (transform.position.y < -limits.y)
		{
			transform.position = new Vector3(transform.position.x, -limits.y, transform.position.z);
		}
		
		if(transform.position.x > limits.x)
		{
			transform.position = new Vector3(limits.x, transform.position.y, transform.position.z);
		}
		else if (transform.position.x < -limits.x)
		{
			transform.position = new Vector3(-limits.x, transform.position.y, transform.position.z);
		}

		if(axis.x>0)
		{
			prop.BlueFire();
		}
		else if(axis.x<0)
        {
			prop.RedFire();
        }

        else {
			prop.Stop();
		}	
	}

    public void SetAxis(Vector2 currentAxis)
    {
        axis = currentAxis;

    }
	
	public void SetAxis(float x, float y)
    {
        axis = new Vector2(x,y);

    }

    public void Shoot()
    {
        if (ShotTime > weapon.GetCadencia())
        {
            ShotTime = 0f;
            weapon.Shoot();
        }
    }
}

