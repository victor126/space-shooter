﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletUpDown : MonoBehaviour
{
    public Vector2 direction;
    public float velocity;
    private float timer = 0f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(direction * velocity * Time.deltaTime);
        timer += Time.deltaTime;
        if (timer > 0.0f)
        {
            direction.x = 1f;
            direction.y = 0f;
        }
        if(timer > 0.5f)
        {
            direction.x = 0f;
            direction.y = 1f;
        }
        if (timer > 1f)
        {
            direction.x = 1f;
            direction.y = 0f;
        }
        if(timer > 1.5f)
        {
            direction.x = 0f;
            direction.y = -1f;
        }
        if(timer > 2f)
        {
            timer = 0f;
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag == "finish")
        {
            Destroy(gameObject);
        }
    }
}
