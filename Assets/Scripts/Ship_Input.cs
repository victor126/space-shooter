﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ship_Input : MonoBehaviour
{
    public Vector2 axis;

    public PlayerBehaviour player;

    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

        //Debug.Log("x:" + axis.x + " y:" + axis.y);

        player.SetAxis(axis);

        if(Input.GetButton("Fire1"))
        {
            player.Shoot();
        }
    }
}
